# OpenPokeBlue

Welcome to OpenPokeBlue, an open-source project dedicated to recreating the classic Pokémon Blue game for Gameboy.

## Project Motivation

OpenPokeBlue is purely a fan project, created by Go developers who have a personal endeavor to reimagine the iconic gameplay of Pokémon Blue using Go and Ebitenengine.

The project does not include any of the original game's assets, such as graphics, music, or sound effects.

OpenPokeBlue is a non-commercial project.
We're not seeking financial gain or attempting to undermine the original creators' work.
It is not intended to compete with or harm the market for the original "Pokémon Blue" game.
Instead, we strive to provide an alternative experience for fans who wish to revisit the game on contemporary platforms.

We believe that projects like OpenPokeBlue contribute positively to the community by offering ways to engage with older games on modern systems. Our aim is to support preservation efforts and provide an outlet for fan creativity.

## Playing OpenPokeBlue

To experience OpenPokeBlue, you will need to possess a legitimate copy of the original Pokémon Blue cartridge.
This requirement ensures that users are exercising their rights to use assets that they own, rather than illegally distributing intellectual property.
Our project aims to preserve and enhance the compatibility of the original game, not to replace it.

To play the game, follow these steps:

1. **Acquire an Original Cartridge:** Obtain a legitimate copy of the original GameBoy Pokémon Blue cartridge.
   
2. **Dump the ROM:** Use a ROM dumper device, such as [CartReader](https://github.com/sanni/cartreader), to extract the ROM image from the cartridge.

3. **Build and Run:** Clone the OpenPokeBlue repository, navigate to the directory, and build/run the game as instructed in the "Getting Started" section. Run it: `go run .` and build it: `go build .`

## Contribution and Feedback

We welcome contributions and feedback from fellow enthusiasts who are intrigued by the potential of remaking classic game mechanics in a modern programming language. If you're interested:

1. Fork the repository and create a new branch.
2. Experiment with enhancements and improvements.
3. Share your insights by submitting a pull request.

Please review our [Contribution Guidelines](CONTRIBUTING.md) for more information.

## License

OpenPokeBlue is released under the [MIT License](LICENSE), allowing you to freely use, modify, and distribute the codebase according to the license's terms.

## Reach Out to Us

If you have questions, ideas, or thoughts to share, we encourage you to open an issue on GitLab or reach out to us via email (*drpaneas at gmail dot com*).

## Acknowledgements

We would like to take a moment to express our sincere appreciation to the original authors, developers, and creators of Pokémon Blue for the Game Boy.
This iconic game holds a special place in the hearts of countless players around the world, and its influence on the gaming landscape is immeasurable.

The vision, creativity, and hard work that went into crafting the original Pokémon Blue game have left an enduring legacy that continues to inspire us to this day.
From the captivating world of Pokémon to the engaging gameplay mechanics, your contributions have shaped the gaming industry and brought joy to millions.

Thank you for inspiring generations of players, developers, and enthusiasts.
Your dedication and innovation have paved the way for projects like ours, and we're honored to pay homage to the legacy you've left behind.

## Legal Notice

Please note that this project is neither developed by, nor endorsed by Game Freak or Nintendo.

It's important to acknowledge that the legality of fan projects can be complex and may vary by jurisdiction.
While we've taken efforts to ensure the project's compatibility and compliance with relevant laws, we are not legal experts.

If you have concerns about the legal aspects of Pokémon Blue or any potential risks, we strongly recommend contacting us directly via email or opening an issue on GitLab.

---

*Disclaimer: OpenPokeBlue is an independent exploration and fan project. It is not affiliated with or endorsed by the official Pokémon franchise or its creators.*