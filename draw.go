package main

import "github.com/hajimehoshi/ebiten/v2"

func (g *Game) Draw(screen *ebiten.Image) {}

func (g *Game) Layout(_, _ int) (int, int) {
	return screenWidth, screenHeight
}
