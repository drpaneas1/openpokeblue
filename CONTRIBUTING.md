## Contributing

If you find something you'd like to fix that's obviously broken, create a branch, commit your code, and submit a pull request.
If it's a new or missing feature you'd like to see, add an issue, and be descriptive!

We use `golangci-lint` to catch lint errors, and we require all contributors to install and use it.
Installation instructions can be found [here](https://golangci-lint.run/usage/install/).